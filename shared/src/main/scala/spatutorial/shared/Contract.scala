package spatutorial.shared

sealed trait ContractStatus
case object ContractStatusActive extends ContractStatus
case object ContractStatusSigned extends ContractStatus
case object ContractStatusTerminated extends ContractStatus

case class Contract(id:String, customer:String, status:ContractStatus, positions:Seq[ContractPos])
case class ContractPos(id:String, amount:String, article:String, price:String)
