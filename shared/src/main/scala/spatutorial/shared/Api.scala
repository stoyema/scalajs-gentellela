package spatutorial.shared

trait Api {
  // message of the day
  def welcomeMsg(name: String): String

  // get Todo items
  def getAllTodos(): Seq[TodoItem]

  // update a Todo
  def updateTodo(item: TodoItem): Seq[TodoItem]

  // delete a Todo
  def deleteTodo(itemId: String): Seq[TodoItem]


  def getAllContracts(customer: Option[String]) : Seq[Contract]
  def updateContract(customer: Option[String], contract: Contract) : Seq[Contract]
  def deleteContract(customer: Option[String], contractId: String) : Seq[Contract]

}
