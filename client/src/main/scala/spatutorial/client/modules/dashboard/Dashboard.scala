package spatutorial.client.modules.dashboard

import scala.language.existentials
import scala.util.Random
import diode.data.Pot
import diode.react._
import japgolly.scalajs.react._
import japgolly.scalajs.react.extra.router.RouterCtl
import japgolly.scalajs.react.vdom.html_<^._
import spatutorial.client.SPAMain.{Loc, TodoLoc}
import spatutorial.client.components._
import spatutorial.client.modules.ContentModule
import spatutorial.client.modules.ContentModule.PageTitleConfig

object Dashboard {

  case class Props(router: RouterCtl[Loc], proxy: ModelProxy[Pot[String]])

  case class State(motdWrapper: ReactConnectProxy[Pot[String]])  // this line requires existentials enabled

  // create dummy data for the chart
  val cp = Chart.ChartProps(
    "Test chart",
    Chart.BarChart,
    ChartData(
      Random.alphanumeric.map(_.toUpper.toString).distinct.take(10),
      Seq(ChartDataset(Iterator.continually(Random.nextDouble() * 10).take(10).toSeq, "Data1"))
    )
  )

  private val panelConfig = Some(
    ContentModule.ContentPanelConfig(
      "Overview",
      Vector(
        ContentModule.SimpleDropdownItem("Toolbox Item 1", TodoLoc, Some(Icon.adjust)),
        ContentModule.SimpleDropdownItem("Toolbox Item 2", TodoLoc, Some(Icon.adjust))
      ),
      collapseable = true,
      closeable = true
    )
  )

  // create the React component for Dashboard
  private val component = ScalaComponent.builder[Props]("Dashboard")
    // create and store the connect proxy in state for later use
    .initialStateFromProps(props => State(props.proxy.connect(m => m)))
    .renderPS { (_, props, state) =>
      ContentModule(
        props.router,
        PageTitleConfig("Dashboard", Some(TodoLoc)),
        panelConfig,
        <.div(
          // header, MessageOfTheDay and chart components
          <.h2("Dashboard"),
          state.motdWrapper(MotdComponent(_)),
          Chart(cp),
          // create a link to the To Do view
          <.div(props.router.link(TodoLoc)("Check your todos!"))
        )
      )
    }
    .build

  def apply(router: RouterCtl[Loc], proxy: ModelProxy[Pot[String]]) = component(Props(router, proxy))
}
