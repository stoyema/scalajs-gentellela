package spatutorial.client.modules.dashboard

import scala.scalajs.concurrent.JSExecutionContext.Implicits.queue

import autowire._
import diode._
import diode.data._
import diode.util._

// needed for AjaxClient autowire call to work
import boopickle.Default._

import spatutorial.client.services.AjaxClient
import spatutorial.shared.Api

object MotdCircuit {

  case class UpdateMotd(potResult: Pot[String] = Empty) extends PotAction[String, UpdateMotd] {
    override def next(value: Pot[String]) = UpdateMotd(value)
  }

  /**
    * Handles actions related to the Motd
    *
    * @param modelRW Reader/Writer to access the model
    */
  class MotdHandler[M](modelRW: ModelRW[M, Pot[String]]) extends ActionHandler(modelRW) {
    implicit val runner = new RunAfterJS

    override def handle = {
      case action: UpdateMotd =>
        val updateF = action.effect(AjaxClient[Api].welcomeMsg("User X").call())(identity _)
        action.handleWith(this, updateF)(PotAction.handler())
    }
  }

}
