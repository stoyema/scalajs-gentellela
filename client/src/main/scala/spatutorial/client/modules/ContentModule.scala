package spatutorial.client.modules

import japgolly.scalajs.react._
import japgolly.scalajs.react.PropsChildren
import japgolly.scalajs.react.extra.router.RouterCtl
import japgolly.scalajs.react.vdom.html_<^.{<, ^, _}
import spatutorial.client.SPAMain.Loc
import spatutorial.client.components.GlobalStyles
import spatutorial.client.components.Icon.Icon


object ContentModule {

  def apply(
    router: RouterCtl[Loc],
    titleConfig: PageTitleConfig,
    panelConfig: Option[ContentPanelConfig],
    children: VdomNode*
  ) = component(Props(router, titleConfig, panelConfig))(children: _*)

  def apply() = component

  case class SimpleDropdownItem(
    label: String,
    location: Loc,
    icon: Option[Icon]
  )

  case class PageTitleConfig(
    title: String,
    searchLoc: Option[Loc] = None
  )

  case class ContentPanelConfig(
    heading: String,
    contextMenu: Vector[SimpleDropdownItem] = Vector.empty,
    collapseable: Boolean = true,
    closeable: Boolean = false
  )

  case class Props(router: RouterCtl[Loc], titleConfig: PageTitleConfig, panelConfig: Option[ContentPanelConfig])

  @inline private def bss = GlobalStyles.bootstrapStyles

  val clearFix = <.div(^.className := "clearfix")

  // Main title bar with optional search field
  @inline def renderTitleBar(props: Props) =
    <.div(^.className := "page-title",
      <.div(^.className := "title_left",
        <.h3(props.titleConfig.title)
      ),
      if (props.titleConfig.searchLoc.isDefined)
        <.div(^.className := "title_right",
          <.div(^.className := "col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search",
            // TODO: Use button component and search callback in Backend component TBD
            <.div(^.className := "input-group",
              <.input(^.`type` := "text", ^.className := "form-control", ^.placeholder := "Search for ..."),
              <.span(^.className := "input-group-btn",
                <.button(^.className := "btn btn-default", ^.`type` := "button", "Go!")
              )
            )
          )
        )
      else
      VdomArray.empty()
    )

  @inline def renderSimpleDropdownItem(props: Props, item: SimpleDropdownItem) =
    <.li(
      <.a(
        ^.href := props.router.urlFor(item.location).value,
        item.icon.map(<.span(^.className := "pull-right", _)).getOrElse(""),
        item.label
      )
    )

  @inline def renderPanelToolbar(props: Props) =
    <.ul(^.className := "nav navbar-right panel_toolbox",
      if (props.panelConfig.get.collapseable)
        <.li(<.a(^.className := "collapse-link", <.i(^.className := "fa fa-chevron-up")))
      else
        VdomArray.empty()
      ,
      if (props.panelConfig.get.contextMenu.isEmpty)
        VdomArray.empty()
      else
        <.li(^.className := "dropdown",
          <.a(^.className := "dropdown-toggle", ^.href := "#", ^.role := "button",
            VdomAttr("data-toggle") := "dropdown", VdomAttr("aria.expanded") := "false",
            <.i(^.className := "fa fa-wrench")
          ),
          <.ul(
            ^.className := "dropdown-menu",
            ^.role := "menu",
            props.panelConfig.get.contextMenu.toTagMod(item => renderSimpleDropdownItem(props, item))
          )
        )
      ,
      if (props.panelConfig.get.closeable)
        <.li(<.a(^.className := "close-link", <.i(^.className := "fa fa-close")))
      else
        VdomArray.empty()
    )



  @inline def renderContentPanel(props: Props, children: PropsChildren) =
    <.div(^.className := "row",
      <.div(^.className := "col-md-12",
        props.panelConfig.map { panelConfig =>
          <.div(^.className := "x_panel",

            // Content title
            <.div(^.className := "x_title",
              <.h2(panelConfig.heading),
              renderPanelToolbar(props),
              clearFix
            ),

            // Content area
            <.div(^.className := "x_content", children)
          ) // x_panel
        }.getOrElse {
          <.div(^.className := "x_content", children)
        }
      ) // col-md-12
    ) // row

  val component = ScalaComponent.builder[Props]("ContentModule")
    .renderPC((_, p, c) =>
      // content
      <.div(^.className := "right_col", ^.role := "main",
        <.div(^.className := "",
          renderTitleBar(p),
          clearFix,
          renderContentPanel(p, c)
        )
      )
    ).build

}
