package spatutorial.client.modules

import diode.react._
import japgolly.scalajs.react._
import japgolly.scalajs.react.extra.router.RouterCtl
import japgolly.scalajs.react.vdom.html_<^._
import org.scalajs.dom.html.{Div, LI}
import spatutorial.client.SPAMain._
import spatutorial.client.components.GlobalStyles
import spatutorial.client.components.Icon.Icon


class LeftNavigation[T] {

  @inline private def bss = GlobalStyles.bootstrapStyles

  case class NavSpec(title: TitleItem, profile: Option[ProfileItem], menu:Seq[GroupItem])
  case class TitleItem(label: Props => VdomNode, icon: Icon, location: Loc)
  case class ProfileItem(label: Props => VdomNode, img: String)
  case class GroupItem(idx: Int, label: Props => VdomNode, items: Seq[MenuItem])
  case class MenuItem(idx: Int, label: Props => VdomNode, icon: Icon, location: Loc, children: Option[Seq[SubMenuItem]] = None)
  case class SubMenuItem(idx: Int, label: Props => VdomNode, location: Loc)

  case class Props(spec: NavSpec, router: RouterCtl[Loc], currentLoc: Loc, proxy: ModelProxy[T])


  class Backend(backendScope: BackendScope[Props, Unit]) {
    // initiate data load for summary information
    // TODO: remove this dependency from navigation to module event OR make configurable
//    def mounted(props: Props) = Callback.when(props.proxy.value.isEmpty)(props.proxy.dispatchCB(RefreshTodos))

    // Left navigation title
    @inline def renderTitleItem(props: Props, item: TitleItem): VdomTagOf[Div] = <.div(
      ^.className := "navbar nav_title",
      ^.border := "0",
      //props.router.link(item.location)(item.icon, " ", item.label(props))
      <.a(^.href := props.router.urlFor(item.location).value, ^.className := "site_title",
        item.icon, " ", <.span(item.label(props))
      )
    )

    @inline def renderProfileItem(props: Props, item: ProfileItem): VdomTagOf[Div] = <.div(
      ^.className := "profile clearfix",
      <.div(^.className := "profile_pic",
        <.img(^.className := "img-circle profile_img", ^.src := item.img, ^.alt := "Alt Img Txt")
      ),
      <.div(^.className := "profile_info",
        <.h2(item.label(props))
      )
    )

    @inline def renderMenu(props: Props, menu: Seq[GroupItem]): VdomTagOf[Div] = <.div(
      ^.id := "sidebar-menu",
      ^.className := "main_menu_side hidden-print main_menu",
      menu.toTagMod(renderGroupItem(props, _))
    )

    @inline def renderGroupItem(props: Props, item: GroupItem): VdomTagOf[Div] = <.div(
      ^.className := "menu_section"
    )(
      <.h3(item.label(props)),
      <.ul(
        ^.className := "nav side-menu",
        item.items.toTagMod(renderMenuItem(props, _))
      )
    )

    @inline def renderMenuItem(props: Props, item: MenuItem): VdomTagOf[LI] = <.li(
      // mark menu <li> as "active" if current loc is contained in items (for correct folding / unfolding behaviour)
      (^.className := "active").when(item.children.exists(_.exists(_.location == props.currentLoc))),
      <.a(item.icon, item.label(props), <.span(^.className := "fa fa-chevron-down")),
      // child menu
      <.ul(
        ^.className := "nav child_menu",
        // mark submenu <ul> as "display:block" if current loc is contained in items (for unfolding of submenu)
        (^.className := "block").when(item.children.exists(_.exists(_.location == props.currentLoc))),
        item.children.whenDefined { cs =>
          cs.toTagMod { subitem =>
            println(s"Item $subitem, current ${props.currentLoc}, equal: " + (props.currentLoc == subitem.location))
            <.li(
              ^.key := subitem.idx,
              (^.className := "current-page").when(props.currentLoc == subitem.location),
              <.a(
                ^.href := props.router.urlFor(subitem.location).value,
                subitem.label(props)
              )
            )
          }
        }
      )
    )

    val clearfix = <.div(^.className := "clearfix")

    def renderItems(props: Props): TagMod = TagMod(
      renderTitleItem(props, props.spec.title),
      clearfix,
      props.spec.profile.map(p => renderProfileItem(props, p)).getOrElse(<.br),
      <.br(),
      renderMenu(props, props.spec.menu)
    )

    def render(props: Props): VdomTagOf[Div] = {
      // TODO: remove
      println("Current location: " + props.currentLoc)

      <.div(^.className := "col-md-3 left_col",
        <.div(^.className := "left_col scroll-view")(
          renderItems(props)
        )
      )
    }
  }

  private val component  = ScalaComponent.builder[Props]("LeftNavigation")
    .renderBackend[Backend]
    //    .componentDidMount(scope => scope.backend.mounted(scope.props))
    .build

  def apply(spec: NavSpec, ctl: RouterCtl[Loc], currentLoc: Loc, proxy: ModelProxy[T]) =
    component(Props(spec, ctl, currentLoc, proxy))

}
