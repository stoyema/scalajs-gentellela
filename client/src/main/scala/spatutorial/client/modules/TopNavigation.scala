package spatutorial.client.modules

import diode.react._
import japgolly.scalajs.react._
import japgolly.scalajs.react.extra.router.RouterCtl
import japgolly.scalajs.react.vdom.html_<^._
import org.scalajs.dom.html.{Div, LI, Span, UList}
import spatutorial.client.SPAMain._
import spatutorial.client.components.GlobalStyles
import spatutorial.client.components.Icon.Icon

/**
  * Created by mstoye on 29.04.17.
  */
class TopNavigation[T] {

  @inline private def bss = GlobalStyles.bootstrapStyles

  case class Props(spec: TopNavSpec, router: RouterCtl[Loc], currentLoc: Loc, proxy: ModelProxy[T])

  // Specification structure root for the top navigation
  case class TopNavSpec(items: Seq[TopnavItem])

  // Items which may appear at the top level in the top navigation
  trait TopnavItem
  // Profile: with image and name, dropdown
  case class NavbarUsermenuItem(label: Props => VdomNode, img: Props => String, alt: Props => String, items: Seq[DropdownMenuItem]) extends TopnavItem
  // Dropdown with dynamic list of items - with icon and optional badge with configurable content
  case class NavbarDropdownItem(label: Props => VdomNode, icon: IconWithBadge, items: Props => Seq[DropdownMenuItem]) extends TopnavItem
  case class IconWithBadge(icon: Icon, info: Props => String, color: Props => String)

  // Menu items in topnav dropdowns
  trait DropdownMenuItem
  // Normal static menu item with label and optional icon
  case class SimpleDropdownItem(label: Props => VdomNode, location: Loc, icon: Option[Icon]) extends DropdownMenuItem
  // Info / message menu item with image, title and content
  case class InfoDropdownItem(title: VdomNode, location: Loc, img: String, time: VdomNode, content: VdomNode) extends DropdownMenuItem

  class Backend(backendScope: BackendScope[Props, Unit]) {
    // initiate data load for summary information
    // TODO: remove this dependency from navigation to module event OR make configurable
    //    def mounted(props: Props) = Callback.when(props.proxy.value.isEmpty)(props.proxy.dispatchCB(RefreshTodos))

    /////////////////////////////////////////////////////////
    // Renderer functions for the top navbar
    /////////////////////////////////////////////////////////

    def render(props: Props): VdomTagOf[Div] = {
      // top navigation
      <.div(^.className := "top_nav",
        <.div(^.className := "nav_menu",
          <.nav(
            <.div(^.className := "nav toggle",
              <.a(^.id := "menu_toggle", <.i(^.className := "fa fa-bars"))
            ),
            renderTopNavbar(props, props.spec)
          ) // nav
        ) // nav-menu
      ) // top_nav
    }

    @inline def renderTopNavbar(props: Props, spec: TopNavSpec): VdomTagOf[UList] = <.ul(
      ^.className := "nav navbar-nav navbar-right",
      props.spec.items.toTagMod(renderTopNavbarItem(props, _))
    )

    /////////////////////////////////////////////////////////
    // Renderer functions for the top navbar entries
    /////////////////////////////////////////////////////////

    @inline def renderTopNavbarItem(props: Props, item: TopnavItem) = item match {
      case i:NavbarUsermenuItem => renderUsermenuTopNavbarItem(props, i)
      case i:NavbarDropdownItem => renderDropdownTopNavbarItem(props, i)
    }

    // top right profile with dropdown menu
    @inline def renderUsermenuTopNavbarItem(props: Props, usermenuItem: NavbarUsermenuItem) = <.li(
      ^.className := "",
      <.a(
        ^.className := "user-profile dropdown-toggle",
        ^.href := "javascript:;",
        VdomAttr("data-toggle") := "dropdown",
        VdomAttr("aria.expanded") := "false",
        <.img(
          ^.src := usermenuItem.img(props),
          ^.alt := usermenuItem.alt(props)),
          usermenuItem.label(props),
        <.span("  "),
          if (usermenuItem.items.nonEmpty) <.span(^.className := "fa fa-angle-down") else ""  // should use .whenDefined conditional ?
      ),
      <.ul(
        ^.className := "dropdown-menu dropdown-usermenu pull-right",
        usermenuItem.items.toTagMod(renderDropdownItem(props, _))
      )
    )

    @inline def renderIconWithBadge(props: Props, iwb: IconWithBadge) : VdomTagOf[Span] = <.span(
      iwb.icon,
      <.span(^.className := s"badge bg-${iwb.color(props)}", iwb.info(props))
    )

    @inline def renderDropdownTopNavbarItem(props: Props, ddmenuItem: NavbarDropdownItem):VdomTagOf[LI] = <.li(
      ^.role := "presentation", ^.className := "dropdown",
      <.a(
        ^.href := "javascript:;",
        ^.className := "dropdown-toggle info-number", VdomAttr("data-toggle") := "dropdown", VdomAttr("aria.expanded") := "false",
        renderIconWithBadge(props, ddmenuItem.icon)
      ),
      <.ul(
        ^.className := "dropdown-menu list-unstyled msg_list", ^.role := "menu",
        ddmenuItem.items(props).toTagMod(renderDropdownItem(props, _))
      )
    )

    /////////////////////////////////////////////////////////
    // Renderer functions for the dropdown menu items
    /////////////////////////////////////////////////////////

    @inline def renderDropdownItems(props: Props, items: Seq[DropdownMenuItem]): VdomTagOf[UList] = <.ul(
      ^.className := "dropdown-menu dropdown-usermenu pull-right",
      items.toTagMod(renderDropdownItem(props, _))
    )

    @inline def renderDropdownItem(props: Props, item: DropdownMenuItem): VdomTagOf[LI] = item match {
      case i:SimpleDropdownItem => renderSimpleDropdownItem(props, i)
      case i:InfoDropdownItem => renderInfoDropdownItem(props, i)
    }

    @inline def renderSimpleDropdownItem(props: Props, item: SimpleDropdownItem): VdomTagOf[LI] = <.li(
      <.a(
        ^.href := props.router.urlFor(item.location).value,
        item.icon.map(<.span(^.className := "pull-right", _)).getOrElse(""),
        item.label(props)
      )
    )

    @inline def renderInfoDropdownItem(props: Props, item: InfoDropdownItem): VdomTagOf[LI] = <.li(
      <.a(
        ^.href := props.router.urlFor(item.location).value,
        <.span(^.className := "image", <.img(^.src := item.img)),
        <.span(
          <.span(item.title),
          <.span(^.className := "time", item.time)
        ),
        <.span(^.className := "message", item.content)
      )
    )

  }

  private val component = ScalaComponent.builder[Props]("TopNavigation")
    .renderBackend[Backend]
//    .componentDidMount(scope => scope.backend.mounted(scope.props))
    .build

  def apply(spec: TopNavSpec, ctl: RouterCtl[Loc], currentLoc: Loc, proxy: ModelProxy[T]) =
    component(Props(spec, ctl, currentLoc, proxy))


}
