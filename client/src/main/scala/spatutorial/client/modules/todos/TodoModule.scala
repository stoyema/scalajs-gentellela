package spatutorial.client.modules.todos

import diode.data._
import diode.react.ReactPot._
import diode.react._
import japgolly.scalajs.react._
import japgolly.scalajs.react.extra.router.RouterCtl
import japgolly.scalajs.react.vdom.html_<^._
import spatutorial.client.SPAMain.Loc
import spatutorial.client.components.Bootstrap.Button
import spatutorial.client.components.Icon
import spatutorial.client.modules.ContentModule
import spatutorial.client.modules.ContentModule.PageTitleConfig
import spatutorial.client.modules.todos.TodoCircuit._
import spatutorial.shared.TodoItem

/**
  * Created by mstoye on 29.04.17.
  */
object TodoModule {

  case class Props(router: RouterCtl[Loc], proxy: ModelProxy[Pot[Todos]])

  case class State(selectedItem: Option[TodoItem] = None, showTodoForm: Boolean = false)

  class Backend($: BackendScope[Props, State]) {
    def mounted(props: Props) =
    // dispatch a message to refresh the todos, which will cause TodoStore to fetch todos from the server
      Callback.when(props.proxy().isEmpty)(props.proxy.dispatchCB(RefreshTodos))

    def editTodo(item: Option[TodoItem]) =
    // activate the edit dialog
      $.modState(s => s.copy(selectedItem = item, showTodoForm = true))

    def todoEdited(item: TodoItem, cancelled: Boolean) = {
      val cb = if (cancelled) {
        // nothing to do here
        Callback.log("Todo editing cancelled")
      } else {
        Callback.log(s"Todo edited: $item") >>
          $.props >>= (_.proxy.dispatchCB(UpdateTodo(item)))
      }
      // hide the edit dialog, chain callbacks
      cb >> $.modState(s => s.copy(showTodoForm = false))
    }

    def render(p: Props, s: State) =
      ContentModule(
        p.router,
        PageTitleConfig("ToDo Items"),
        None,
        <.div(
          p.proxy().renderFailed(ex => "Error loading"),
          p.proxy().renderPending(_ > 500, _ => "Loading..."),
          p.proxy().render(
            todos => TodoList(
              todos.items,   // items to render
              item => p.proxy.dispatchCB(UpdateTodo(item)), // state change
              item => editTodo(Some(item)),                 // edit item
              item => p.proxy.dispatchCB(DeleteTodo(item))  // delete item
            )
          ),
          Button(Button.Props(editTodo(None)), Icon.plusSquare, " New")
        ),
        // if the dialog is open, add it to the panel
        if (s.showTodoForm)
          TodoForm(TodoForm.Props(s.selectedItem, todoEdited))
        else // otherwise add an empty placeholder
          VdomArray.empty()
      )
  }

  // create the React component for To Do management
  val component = ScalaComponent.builder[Props]("TODO")
    .initialState(State()) // initial state from TodoStore
    .renderBackend[Backend]
    .componentDidMount(scope => scope.backend.mounted(scope.props))
    .build

  /** Returns a function compatible with router location system while using our own props */
  def apply(router: RouterCtl[Loc], proxy: ModelProxy[Pot[Todos]]) =
    component(Props(router, proxy))
}


