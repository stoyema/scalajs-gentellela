package spatutorial.client.modules.contracts

import japgolly.scalajs.react.vdom.html_<^._
import japgolly.scalajs.react.{BackendScope, Callback, ScalaComponent}
import japgolly.scalajs.react._
import scalacss.ScalaCssReact._
import spatutorial.client.components.Bootstrap.{Button, Modal}
import spatutorial.client.components.{GlobalStyles, Icon}
import spatutorial.client.logger.log
import spatutorial.shared.{Contract, ContractStatusActive, ContractStatusSigned, ContractStatusTerminated}


object ContractForm {
  // shorthand for styles
  @inline private def bss = GlobalStyles.bootstrapStyles

  case class Props(item: Option[Contract], submitHandler: (Contract, Boolean) => Callback)

  case class State(item: Contract, cancelled: Boolean = true)

  class Backend(t: BackendScope[Props, State]) {

    def submitForm(): Callback = {
      // mark it as NOT cancelled (which is the default)
      t.modState(s => s.copy(cancelled = false))
    }

    def formClosed(state: State, props: Props): Callback = {
      // call parent handler with the new item and whether form was OK or cancelled
      props.submitHandler(state.item, state.cancelled)
    }

    def updateId(e: ReactEventFromInput) = {
      val text = e.target.value
      t.modState(s => s.copy(item = s.item.copy(id = text)))
    }

    // update customer
    def updateCustomer(e: ReactEventFromInput) = {
      val text = e.target.value
      t.modState(s => s.copy(item = s.item.copy(customer = text)))
    }

    // update contract status
    def updateStatus(e: ReactEventFromInput) = {
      val newStat = e.currentTarget.value match {
        case p if p == ContractStatusActive.toString => ContractStatusActive
        case p if p == ContractStatusSigned.toString => ContractStatusSigned
        case p if p == ContractStatusTerminated.toString => ContractStatusTerminated
      }
      t.modState(s => s.copy(item = s.item.copy(status = newStat)))
    }

    def render(p: Props, s: State) = {
      log.debug(s"User is ${if (s.item.id == "") "adding" else "editing"} a contract")
      val headerText = if (s.item.id == "") "Add new todo" else "Edit todo"
      Modal(Modal.Props(
        // header contains a cancel button (X)
        header = hide => <.span(<.button(^.tpe := "button", bss.close, ^.onClick --> hide, Icon.close), <.h4(headerText)),
        // footer has the OK button that submits the form before hiding it
        footer = hide => <.span(Button(Button.Props(submitForm() >> hide), "OK")),
        // this is called after the modal has been hidden (animation is completed)
        closed = formClosed(s, p)),
        <.div(bss.formGroup,
          // ID
          <.label(^.`for` := "contractId", "Contract ID"),
          <.input.text(bss.formControl, ^.id := "contractId", ^.value := s.item.id,
            ^.placeholder := "ID", ^.onChange ==> updateId)),

        <.div(bss.formGroup,
          // Customer
          <.label(^.`for` := "customer", "Customer"),
          <.input.text(bss.formControl, ^.id := "customer", ^.value := s.item.customer,
            ^.placeholder := "write description", ^.onChange ==> updateCustomer)),

        <.div(bss.formGroup,
          <.label(^.`for` := "status", "Status"),
          // using defaultValue = "Normal" instead of option/selected due to React
          <.select(bss.formControl, ^.id := "state", ^.value := s.item.status.toString, ^.onChange ==> updateStatus,
            <.option(^.value := ContractStatusActive.toString, "Active"),
            <.option(^.value := ContractStatusSigned.toString, "Signed"),
            <.option(^.value := ContractStatusTerminated.toString, "Terminated")
          )
        )
      )
    }
  }

  val component = ScalaComponent.builder[Props]("TodoForm")
    .initialStateFromProps(p => State(p.item.getOrElse(Contract("", "", ContractStatusSigned, Seq()))))
    .renderBackend[Backend]
    .build

  def apply(props: Props) = component(props)
}
