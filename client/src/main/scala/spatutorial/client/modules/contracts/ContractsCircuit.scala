package spatutorial.client.modules.contracts

import scala.scalajs.concurrent.JSExecutionContext.Implicits.queue

import autowire._
import diode._
import diode.data._

// needed for AjaxClient autowire call to work
import boopickle.Default._

import spatutorial.client.logger.log
import spatutorial.client.services._
import spatutorial.shared._

object ContractsCircuit {

  case class RefreshContracts(customer: Option[String]) extends Action

  case class UpdateContracts(customer: Option[String], contracts: Seq[Contract]) extends Action

  case class UpdateContract(contract: Contract) extends Action

  case class DeleteContract(contract: Contract) extends Action

  case class Contracts(customer: Option[String], contracts: Seq[Contract]) {
    def updated(newContract: Contract) = {
      contracts.indexWhere(_.id == newContract.id) match {
        case -1 => Contracts(customer, contracts :+ newContract)
        case idx => Contracts(customer, contracts.updated(idx, newContract))
      }
    }

    def removed(contract: Contract) = Contracts(customer, contracts.filterNot(_.id == contract.id))
  }

  class ContractsHandler[M](modelRW: ModelRW[M, Pot[Contracts]]) extends ActionHandler(modelRW) {
    override def handle = {
      case e@RefreshContracts(c) =>
        println("Event: " + e)
        effectOnly(
          Effect(AjaxClient[Api].getAllContracts(c).call().map(UpdateContracts(c, _)))
        )
      case e@UpdateContracts(cust, con) =>
        println("Event: " + e)
        updated(
          Ready(Contracts(cust, con))
        )
      case e@UpdateContract(con) =>
        println("Event: " + e)
        updated(
          value.map(_.updated(con)),
          Effect(AjaxClient[Api].updateContract(value.get.customer, con).call().map(UpdateContracts(value.get.customer, _)))
        )

      case e@DeleteContract(con) =>
        println("Event: " + e)
        updated(
          value.map(_.removed(con)),
          Effect(AjaxClient[Api].deleteContract(value.get.customer, con.id).call().map(UpdateContracts(value.get.customer, _)))
        )
    }
  }

}
