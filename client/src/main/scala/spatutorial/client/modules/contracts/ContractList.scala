package spatutorial.client.modules.contracts

import scalacss.ScalaCssReact._

import japgolly.scalajs.react.{Callback, ScalaComponent}
import japgolly.scalajs.react.vdom.html_<^._

import spatutorial.client.components.Bootstrap.{Button, CommonStyle}
import spatutorial.client.components.GlobalStyles
import spatutorial.shared.{Contract, ContractStatusActive, ContractStatusSigned, ContractStatusTerminated}

object ContractList {
  @inline private def bss = GlobalStyles.bootstrapStyles

  case class ContractListProps(
    items: Seq[Contract],
    stateChange: Contract => Callback,
    editItem: Contract => Callback,
    deleteItem: Contract => Callback
  )

  private val ContractList = ScalaComponent.builder[ContractListProps]("ContractList")
    .render_P(p => {
      val style = bss.listGroup
      def renderItem(item: Contract):VdomElement = {
        val statusText = item.status match {
          case ContractStatusActive => ("Active", style.itemOpt(CommonStyle.success))
          case ContractStatusSigned => ("Signed", style.itemOpt(CommonStyle.info))
          case ContractStatusTerminated => ("Terminated", style.itemOpt(CommonStyle.warning))
        }
        <.li(statusText._2,
          <.input.checkbox(^.checked := item.customer=="bla", ^.onChange --> p.stateChange(item.copy(customer = "bla"))),
          <.span(" "),
          <.span(item.id),
          <.span(" "),
          <.span(item.customer),
          <.span(" "),
          <.span(statusText._1),
          Button(Button.Props(p.editItem(item), addStyles = Seq(bss.pullRight, bss.buttonXS)), "Edit"),
          Button(Button.Props(p.deleteItem(item), addStyles = Seq(bss.pullRight, bss.buttonXS)), "Delete")
        )
      }
      <.ul(style.listGroup)(p.items map renderItem:_*)
    })
    .build

  def apply(
     items: Seq[Contract],
     stateChange: Contract => Callback,
     editItem: Contract => Callback,
     deleteItem: Contract => Callback
   ) = ContractList(ContractListProps(items, stateChange, editItem, deleteItem))

}

