package spatutorial.client.modules.contracts

import diode.data._
import diode.react.ReactPot._
import diode.react._
import japgolly.scalajs.react._
import japgolly.scalajs.react.vdom.html_<^._
import spatutorial.client.components.Bootstrap.{Button, Panel}
import spatutorial.client.components.{GlobalStyles, Icon}
import spatutorial.client.logger.log
import spatutorial.shared._
import ContractsCircuit._
import japgolly.scalajs.react.extra.router.RouterCtl
import japgolly.scalajs.react.raw.ReactElement
import spatutorial.client.SPAMain.Loc
import spatutorial.client.modules.ContentModule
import spatutorial.client.modules.ContentModule.PageTitleConfig

object ContractsModule {

  case class Props(router: RouterCtl[Loc], proxy: ModelProxy[Pot[Contracts]])

  case class State(selectedContract: Option[Contract] = None, selectedPos: Option[ContractPos] = None, showForm: Boolean = false)

  class Backend($: BackendScope[Props, State]) {
    def mounted(props: Props) =
    // dispatch a message to refresh the todos, which will cause TodoStore to fetch todos from the server
      Callback.when(props.proxy().isEmpty)(props.proxy.dispatchCB(RefreshContracts(None)))

    def editContract(item: Option[Contract]) =
    // activate the edit dialog
      $.modState(s => s.copy(selectedContract = item, showForm = true))

    def contractEdited(item: Contract, cancelled: Boolean) = {
      val cb = if (cancelled) {
        // nothing to do here
        Callback.log("Contract editing cancelled")
      } else {
        Callback.log(s"Contract edited: $item") >>
          $.props >>= (_.proxy.dispatchCB(UpdateContract(item)))
      }
      // hide the edit dialog, chain callbacks
      cb >> $.modState(s => s.copy(showForm = false))
    }

    def render(p: Props, s: State) = {
      ContentModule(
        p.router,
        PageTitleConfig("Contracts"),
        None,
        <.div(
          p.proxy().renderFailed(ex => "Error loading"),
          p.proxy().renderPending(_ > 500, _ => "Loading..."),
          p.proxy().render(
            contracts => ContractList(
              contracts.contracts,
              contract => p.proxy.dispatchCB(UpdateContract(contract)),
              contract => editContract(Some(contract)),
              contract => p.proxy.dispatchCB(DeleteContract(contract))
            )
          ),
          Button(Button.Props(editContract(None)), Icon.plusSquare, " New")
        ),
        // if the dialog is open, add it to the panel
        if (s.showForm)
          ContractForm(ContractForm.Props(s.selectedContract, contractEdited))
        else
          VdomArray.empty()
      )
    }

  }

  // create the React component for To Do management
  val component = ScalaComponent.builder[Props]("Contracts")
    .initialState(State()) // initial state from TodoStore
    .renderBackend[Backend]
    .componentDidMount(scope => scope.backend.mounted(scope.props))
    .build

  /** Returns a function compatible with router location system while using our own props */
  def apply(router: RouterCtl[Loc], proxy: ModelProxy[Pot[Contracts]]) =
    component(Props(router, proxy))
}

