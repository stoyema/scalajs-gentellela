package spatutorial.client.modules.test

import scala.concurrent.Future
import scala.scalajs.concurrent.JSExecutionContext.Implicits.queue

import diode._
import diode.data._

object TestCircuit {

  case class UpdateTest(potResult: Pot[String] = Empty) extends PotAction[String, UpdateTest] {
    override def next(value: Pot[String]) = UpdateTest(value)
  }

  class TestHandler[M](modelRW: ModelRW[M, Pot[String]]) extends ActionHandler(modelRW) {
    override def handle = {
      case action: UpdateTest =>
        val updateTest = action.effect( Future { "Test" } )(identity _)
        action.handleWith(this, updateTest)(PotAction.handler())
    }
  }

}
