package spatutorial.client.modules.test

import diode.data._
import diode.react._
import japgolly.scalajs.react._
import japgolly.scalajs.react.vdom.html_<^._
import spatutorial.client.components.Bootstrap.{Button, Panel}
import spatutorial.client.components.Icon

object TestModule {

  case class Props(proxy: ModelProxy[Pot[String]])

  case class State(color:String = "green")

  class Backend($: BackendScope[Props, State]) {
    //def mounted(props: Props) = {}
    // dispatch a message to refresh the todos, which will cause TodoStore to fetch todos from the server
    // Callback.when(props.proxy().isEmpty)(props.proxy.dispatchCB(RefreshTest))

    def colorToggle(c:String) : String = c match {
      case "green" => "red"
      case "red" => "orange"
      case "orange" => "green"
    }

    def clickTest() =
      // perform click change
      $.modState(s => s.copy(color = colorToggle(s.color)))

    def render(p: Props, s: State) = {
      val n = <.div(^.color := s.color, <.h3(p.proxy().get))
      Panel(
        Panel.Props("Test !"),
        n,
        Button(Button.Props(clickTest()), Icon.plusSquare, " Click !")
      )
    }
  }

  // create the React component for To Do management
  val component = ScalaComponent.builder[Props]("TEST")
    .initialState(State()) // initial state
    .renderBackend[Backend]
//    .componentDidMount(scope => scope.backend.mounted(scope.props))
    .build


  /** Returns a function compatible with router location system while using our own props */
  def apply(proxy: ModelProxy[Pot[String]]) = component(Props(proxy))

}
