package spatutorial.client.components

import spatutorial.client.CssSettings._

object GlobalStyles extends StyleSheet.Inline {
  val bootstrapStyles = new BootstrapStyles
}
