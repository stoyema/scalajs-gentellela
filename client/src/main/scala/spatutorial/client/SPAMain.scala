package spatutorial.client

import diode.data.{Pot, Ready}
import diode.react.ReactConnectProxy
import diode.react.ReactPot._
import japgolly.scalajs.react.extra.router._
import japgolly.scalajs.react.vdom.html_<^._
import org.scalajs.dom
import spatutorial.client.CssSettings._
import scalacss.ScalaCssReact._
import spatutorial.client.components.{GlobalStyles, Icon}
import spatutorial.client.logger._
import spatutorial.client.modules.contracts.ContractsCircuit.Contracts
import spatutorial.client.modules.contracts.ContractsModule
import spatutorial.client.modules.dashboard.Dashboard
import spatutorial.client.modules.test.TestModule
import spatutorial.client.modules.todos.TodoCircuit.Todos
import spatutorial.client.modules.todos.TodoModule
import spatutorial.client.modules.{TopNavigation, _}
import spatutorial.client.services.{RootModel, SPACircuit}
import spatutorial.shared.{TodoHigh, TodoLow, TodoNormal}

import scala.scalajs.js
import scala.scalajs.js.annotation.{JSExport, JSExportTopLevel}

@JSExportTopLevel("SPAMain")
object SPAMain extends js.JSApp {

  // Define the locations (pages) used in this application
  sealed trait Loc
  case object RootLoc extends Loc
  case object DashboardLoc extends Loc
  case object TodoLoc extends Loc
  case object TestLoc extends Loc
  case object ContractLoc extends Loc

  // configure the router
  val routerConfig = RouterConfigDsl[Loc].buildConfig { dsl =>
    import dsl._

    val todoWrapper = SPACircuit.connect[Pot[Todos]]((s:RootModel) => s.todos)
    val testWrapper = SPACircuit.connect[Pot[String]]((s:RootModel) => s.test)
    val contractsWrapper = SPACircuit.connect[Pot[Contracts]]((s:RootModel) => s.contracts)

    // wrap/connect components to the circuit
    (
        staticRoute(root, RootLoc)                ~> renderR(ctl => SPACircuit.wrap(_.motd)(proxy => Dashboard(ctl, proxy)))
      | staticRoute("#dash", DashboardLoc)        ~> renderR(ctl => SPACircuit.wrap(_.motd)(proxy => Dashboard(ctl, proxy)))
      | staticRoute("#todo", TodoLoc)             ~> renderR(ctl => todoWrapper(proxy => TodoModule(ctl, proxy)))
      | staticRoute("#test", TestLoc)             ~> renderR(ctl => testWrapper(proxy => TestModule(proxy)))
      | staticRoute("#contract", ContractLoc)     ~> renderR(ctl => contractsWrapper(proxy => ContractsModule(ctl, proxy)))
    ).notFound(
        redirectToPage(DashboardLoc)(Redirect.Replace)
    )
  }.renderWith(layout)

  // specify left navigation
  object ThisLeftNav extends LeftNavigation[NavModel]
  import ThisLeftNav._
  val leftNavSpec = NavSpec(
    TitleItem(_ => "RDM Console", Icon.paw, ContractLoc),
    Some(ProfileItem(_ => "John Doe", "assets/images/demo/img.jpg")),
    Seq(
      GroupItem(0, p => p.proxy().todos.render(c => "DIMENSION 1 [" + c.items.size + " TODOs]"),
        Seq(
          MenuItem(1, _ => "Home", Icon.home, DashboardLoc,
            Some(Seq(
              SubMenuItem(11, _ => "Dashboard 1", DashboardLoc),
              SubMenuItem(12, _ => "ToDo", TodoLoc)
            ))
          ),
          MenuItem(2, _ => "Test", Icon.android, TestLoc,
            Some(Seq(
              SubMenuItem(21, _ => "Some app", TestLoc),
              SubMenuItem(22, _ => "Contracts", ContractLoc)
            ))
          ),
          MenuItem(3, _ => "Contracts", Icon.calculator, ContractLoc)
        )
      ),
      GroupItem(4, p => p.proxy().contractCount.render(c => "DIMENSION 2 [" + c + " Contracts]"),
        Seq(
          MenuItem(5, _ => "Home", Icon.home, DashboardLoc,
            Some(Seq(
              SubMenuItem(51, _ => "Dashboard 1", DashboardLoc),
              SubMenuItem(52, _ => "ToDo", TodoLoc)
            ))
          ),
          MenuItem(6, _ => "Test", Icon.android, TestLoc,
            Some(Seq(
              SubMenuItem(61, _ => "Some app", TestLoc),
              SubMenuItem(62, _ => "Contracts", ContractLoc)
            ))
          )
        )
      )
    )
  )

  object ThisTopNav extends TopNavigation[NavModel]
  import ThisTopNav._
  val topNavSpec = {
    TopNavSpec(
      Seq(
        NavbarUsermenuItem(_ => <.span("John Dooe"), _ => "assets/images/demo/img.jpg", _ => "John Doooe",
          Seq(
            SimpleDropdownItem(_ => "Profile", DashboardLoc, None),
            SimpleDropdownItem(_ => "Settings", TodoLoc, Some(Icon.tachometer)),
            SimpleDropdownItem(_ => "Help", ContractLoc, Some(Icon.question)),
            SimpleDropdownItem(_ => "Log out", DashboardLoc, Some(Icon.signOut))
          )
        ),
        NavbarDropdownItem(
          _ => "Messages",
          IconWithBadge(
            Icon.envelopeO,
            p => if (p.proxy().todos.isReady) p.proxy().todos.get.items.size.toString else "...",
            _ => "green"
          ),
          p => p.proxy().todos match {
            case Ready(todos) => todos.items.map(todo =>
              InfoDropdownItem(
                todo.priority match {
                  case TodoLow => <.span(^.className := "label label-info", "Prio: Low")
                  case TodoNormal => <.span(^.className := "label label-warning", "Prio: Med")
                  case TodoHigh => <.span(^.className := "label label-danger", "Prio: High")
                },
                TodoLoc,
                "assets/images/demo/img.jpg",
                if (todo.completed) Icon.bomb else "",
                todo.content
              )
            )
            case _ => Seq()
          }
        )
      )
    )
  }

  // map model to navigation: bind e.g. size of todos to int value.
  // Value is accessible through e.g.: p => p.proxy().render(c => "Blabla " + c) producing a ReactNode
  //  val navWrapper : ReactConnectProxy[Pot[Int]] = SPACircuit.connect[Pot[Int]]((s:RootModel) => s.todos.map(_.items.size))
  // or shorter: val navWrapper = SPACircuit.connect(_.todos.map(_.items.size))
  //val navWrapper : ReactConnectProxy[Pot[NavModel]]= SPACircuit.connect((s:RootModel) => s.map(_ => NavModel(s.todos.map(_.items), s.todos.map(_.items.size))))
  // wrapper is readonly version
  case class NavModel(todos:Pot[Todos], contractCount:Pot[Int])
  val navWrapper : ReactConnectProxy[NavModel] = SPACircuit.connect((s:RootModel) => NavModel(s.todos, s.contracts.map(t => t.contracts.size)))

  // define root layout
  def layout(c: RouterCtl[Loc], r: Resolution[Loc]) = {
      <.div(^.className := "main_container",

        // left navigation wrapper for proxy binding
        navWrapper(proxy => ThisLeftNav(leftNavSpec, c, r.page, proxy)),

        // top navigation wrapper for proxy binding
        navWrapper(proxy => ThisTopNav(topNavSpec, c, r.page, proxy)),

        // render main content as selected by route
        r.render(),

        // footer
        <.footer(
          <.div(^.className := "pull-right", "Footer Text"),
          <.div(^.className := "clearfix")
        ) // footer
      )
  }


  @JSExport
  def main(): Unit = {
    log.warn("Application starting")
    // send log messages also to the server
    log.enableServerLogging("/logging")
    log.info("This message goes to server as well")

    // create stylesheet
    GlobalStyles.addToDocument()
    // create the router
    val router = Router(BaseUrl.until_#, routerConfig)
    // tell React to render the router in the document body
    router().renderIntoDOM(dom.document.getElementById("root"))
  }
}
