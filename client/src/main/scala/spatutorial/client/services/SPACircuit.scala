package spatutorial.client.services

import diode._
import diode.data._
import diode.react.ReactConnector

import spatutorial.client.modules.contracts.ContractsCircuit._
import spatutorial.client.modules.todos.TodoCircuit._
import spatutorial.client.modules.dashboard.MotdCircuit._
import spatutorial.client.modules.test.TestCircuit._

// The base model of our application
case class RootModel(todos: Pot[Todos], motd: Pot[String], test: Pot[String], contracts: Pot[Contracts])

// Application circuit
object SPACircuit extends Circuit[RootModel] with ReactConnector[RootModel] {
  // initial application model
  override protected def initialModel = RootModel(Empty, Empty, Empty, Empty)

  // combine all handlers into one
  override protected val actionHandler = composeHandlers(
    new TodoHandler(zoomRW(_.todos)((m, v) => m.copy(todos = v))),
    new MotdHandler(zoomRW(_.motd)((m, v) => m.copy(motd = v))),
    new TestHandler(zoomRW(_.test)((m, v) => m.copy(test = v))),
    new ContractsHandler(zoomRW(_.contracts)((m, v) => m.copy(contracts = v)))
  )
}
